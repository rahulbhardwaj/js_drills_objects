function invert(testObject){
    if (!testObject || testObject.constructor != Object){
        return {};
    }
    answer = {}
    for (key in testObject){
        answer[testObject[key]] = key;
    }
    return answer;
}
module.exports = invert;