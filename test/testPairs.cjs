// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs

let testObject = require('../objects.cjs');
let pairs = require('../pairs.cjs');

console.log(pairs(testObject));
console.log(pairs({}));
console.log(pairs(['array example']));
console.log(pairs());