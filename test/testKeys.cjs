// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.
// Based on http://underscorejs.org/#keys

let testObject = require('../objects.cjs');
let keys = require('../keys.cjs');

console.log(keys(testObject));
console.log(keys());
console.log(keys({}));
console.log(keys(['example array']));