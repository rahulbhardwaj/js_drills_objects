// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.
// http://underscorejs.org/#invert

let testObject = require('../objects.cjs');
let invert = require('../invert.cjs');

console.log(invert(testObject));
console.log(invert({}));
console.log(invert(['array example']));
console.log(invert());