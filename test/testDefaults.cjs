// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults

let testObject = require('../objects.cjs');
let defaults = require('../defaults.cjs');

let defaultProps = {name:'tom',place:'nyc',room:5};

console.log(defaults(testObject,defaultProps));
console.log(defaults());
console.log(defaults(testObject));
console.log(defaults({},{}));
console.log(defaults({},defaultProps));
console.log(defaults(testObject,{}))
console.log(defaults(testObject,['array']));
console.log(defaults(['array'],defaultProps));
console.log(defaults(['array'],['array']));