// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject

let testObject = require('../objects.cjs');
let mapObject = require('../mapObject.cjs');

function add(a,b){
    return a+b;
}

console.log(mapObject(testObject,(a) => a+5));
console.log(mapObject(testObject,(a) => null));
console.log(mapObject(testObject,add));
console.log(mapObject({}));
console.log(mapObject(['array example']));
console.log(mapObject());