// Return all of the values of the object's own properties.
// Ignore functions
// http://underscorejs.org/#values

let testObject = require('../objects.cjs');
let values = require('../values.cjs');

console.log(values(testObject));
console.log(values());
console.log(values({}));
console.log(values(['example array']));
console.log(values({myFunction : function(){return 'Hello World!'}}));