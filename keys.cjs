function keys(testObject){
    if (!testObject || (testObject.constructor !== Object && testObject.constructor !== Array)){
        return [];
    }
    let answer = [];
    for (const key in testObject){
        answer.push(key);
    }
    return answer;
}
module.exports = keys;