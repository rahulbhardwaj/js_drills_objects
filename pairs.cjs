function pairs(testObject){
    if (!testObject || testObject.constructor != Object){
        return [];
    }
    answer = [];
    for(key in testObject){
        answer.push([key,testObject[key]]);
    }
    return answer;
}
module.exports = pairs;