function values(testObject){
    if (!testObject || testObject.constructor != Object){
        return [];
    }
    let answer = [];
    for (const key in testObject){
        if(testObject[key].constructor === Function){
            continue;
        }
        answer.push(testObject[key]);
    }
    return answer;
}
module.exports = values;