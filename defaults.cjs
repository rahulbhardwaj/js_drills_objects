function defaults(testObject,defaultProps){
    if((!testObject && !defaultProps)||(testObject.constructor!== Object && defaultProps.constructor!== Object)){
        return {};
    }
    if(!testObject || testObject.constructor !== Object){
        return defaultProps;
    }
    if(!defaultProps || defaultProps.constructor!= Object){
        return testObject; 
    }
    let keys_testObject = [];
    for(let key in testObject){
        keys_testObject.push(key);
    }
    for(let key in defaultProps){
        if(!(key in keys_testObject)){
            testObject[key] = defaultProps[key];
        }
    }
    return testObject;
}
module.exports = defaults;