function mapObject(testObject,cb){
    if (!testObject || testObject.constructor != Object){
        return {};
    }
    if(!cb){
        return testObject;
    }
    answer = {};
    for(key in testObject){
        answer[key] = cb(testObject[key]);
    }
    return answer;
}
module.exports = mapObject;